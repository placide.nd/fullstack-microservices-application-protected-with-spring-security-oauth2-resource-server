export class Address {
  addressId!: string;
  num!: number;
  street!: string;
  pb!: number;
  city!: string;
  country!: string;
}
