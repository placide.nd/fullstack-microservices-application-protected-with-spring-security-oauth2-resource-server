export enum Type {
  CLIENT = "CLIENT",
  PROSPECT = "PROSPECT",
  ESN = "ESN"
}
