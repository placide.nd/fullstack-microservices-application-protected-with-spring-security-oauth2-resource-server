package com.placide.k8skafkaavroaepccleanarchibsmicrosaddress.domain.exceptions;

public class AddressCityNotFoundException extends Exception {
    public AddressCityNotFoundException(String message) {
        super(message);
    }
}
