package com.placide.k8skafkaavroaepccleanarchibsmicroscompany.domain.exceptions;

public class CompanyEmptyFieldsException extends Exception {
    public CompanyEmptyFieldsException(String message) {
        super(message);
    }
}
