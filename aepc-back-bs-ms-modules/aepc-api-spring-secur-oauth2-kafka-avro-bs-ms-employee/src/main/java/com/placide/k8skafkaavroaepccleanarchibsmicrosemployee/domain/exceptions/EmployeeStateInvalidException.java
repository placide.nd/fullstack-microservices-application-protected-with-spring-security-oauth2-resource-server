package com.placide.k8skafkaavroaepccleanarchibsmicrosemployee.domain.exceptions;

public class EmployeeStateInvalidException extends Exception {
    public EmployeeStateInvalidException(String message) {
        super(message);
    }
}
