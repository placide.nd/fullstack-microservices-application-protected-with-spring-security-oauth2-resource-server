package com.placide.k8skafkaavroaepccleanarchibsmicrosproject.domain.exceptions;

public class ProjectStateInvalidException extends Exception {
    public ProjectStateInvalidException(String message) {
        super(message);
    }
}
